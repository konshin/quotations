//
//  UIColor+AppColors.swift
//  exness
//
//  Created by Konshin on 04.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Тинт
    static let tint = UIColor(red: 254 / 255, green: 206 / 255, blue: 47 / 255, alpha: 1)
    
    /// Цвет фона
    static let background = UIColor(red: 34 / 255, green: 30 / 255, blue: 32 / 255, alpha: 1)
    
    /// Цвет шрифтов
    static let text = tint
    
}
