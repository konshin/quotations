//
//  SocketService.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import Foundation
import Starscream

/// Протокол общения с внешним миром
protocol SocketServiceDelegate: class {
    
    /// Получено сообщение от сервера
    ///
    /// - Parameters:
    ///   - service: Ссылка на сервис
    ///   - response: Сообщение
    func socketServiceDidReceive(_ service: SocketService, response: SocketService.Response)
}

/// Сервис по работе с сокетами
class SocketService {
    
    /// Сокет
    private let socket: WebSocket
    
    /// Последний список подписанных типов
    private var lastSubscribedTypes: [QuotationType]?
    
    /// Декордер
    private let decoder = JSONDecoder()

    init(socketURL: URL) {
        socket = WebSocket(url: socketURL)
        socket.delegate = self
    }
    
    // MARK: - properties
    
    /// Делегат
    private weak var delegate: SocketServiceDelegate?
    
    // MARK: - actions
    
    /// Запустить соединение
    ///
    /// - Parameters:
    ///   - delegate: Делегация над работой
    ///   - onComplete: Блок завершения
    func start(delegate: SocketServiceDelegate, onComplete: @escaping () -> Void) {
        self.delegate = delegate
        connect(onComplete: onComplete)
    }
    
    /// Остановить соединение
    func stop() {
        socket.disconnect()
    }
    
    /// Создать подписку
    ///
    /// - Parameters:
    ///   - types: Типы котировок
    ///   - responseHandler: Хендлер обработки ответа
    func subscribe(types: [QuotationType]) {
        let action = { [weak self] in
            let typesString = types.map { $0.key }
                .joined(separator: ",")
            let action = "SUBSCRIBE: \(typesString)"
            self?.sendAction(action) { [weak self] in
                self?.lastSubscribedTypes = types
            }
        }
        /// Если есть текущая подписка - сначала омтеняем ее
        if let lastTypes = lastSubscribedTypes {
            unsubscribe(types: lastTypes, onComplete: action)
        } else {
            action()
        }
    }
    
    /// Отказаться от подписки
    ///
    /// - Parameters:
    ///   - types: Типы котировок
    ///   - onComplete: Коллбек на завершение
    func unsubscribe(types: [QuotationType], onComplete: (() -> Void)?) {
        let typesString = types.map { $0.key }
            .joined(separator: ",")
        let action = "UNSUBSCRIBE: \(typesString)"
        sendAction(action, onComplete: onComplete)
    }
    
    /// Отправить событие
    ///
    /// - Parameter action: Событие
    private func sendAction(_ action: String, onComplete: (() -> Void)? = nil) {
        let action: () -> Void = { [weak self] in
            self?.socket.write(string: action) {
                onComplete?()
            }
        }
        if !socket.isConnected {
            connect(onComplete: action)
        } else {
            action()
        }
        
    }
    
    /// Подключить сокет
    ///
    /// - Parameter onComplete: Коллбек по завершению
    private func connect(onComplete: (() -> Void)?) {
        if onComplete != nil {
            socket.onConnect = { [weak self] in
                self?.socket.onConnect = nil
                onComplete?()
            }
        }
        socket.connect()
    }
    
}

// MARK: - extra
extension SocketService {
    
    /// Ответ сокета
    ///
    /// - success: Успешный с котировками
    /// - error: Ошика
    enum Response {
        case success([QuotationTick])
        case error(Error)
    }

}

// MARK: - WebSocketDelegate
extension SocketService: WebSocketDelegate {
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocketDidConnect")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocketDidDisconnect: \(String(describing: error))")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        guard let data = text.data(using: String.Encoding.utf8) else {
            return
        }
        
        /// Овтет является первым по подписке
        let isInitialResponse: Bool = text.contains("subscribed_count")
        
        do {
            let list: [QuotationTick]
            if isInitialResponse {
                list = (try decoder.decode(QuotationResponse.self, from: data).subscribedList.ticks) ?? []
            } else {
                list = (try decoder.decode(QuotationSubscribeList.self, from: data).ticks) ?? []
            }
            delegate?.socketServiceDidReceive(self, response: .success(list))
        } catch {
            delegate?.socketServiceDidReceive(self,
                                              response: .error(error))
            return
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        return
    }
    
}
