//
//  QuotationsDisplayController.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import UIKit

/// Ключ хранения в кеше выбранных типов
private let selectedTypesCacheKey = "selectedTypesCacheKey"
/// Идентификатор для ячейки
private let reuseIdentifier = "reuseIdentifier"

/// Экран отображения котировок по подписке
class QuotationsDisplayController: UITableViewController {
    
    /// Хранилище
    private let storage: Storage
    /// Сервис по работе с сокетами
    private let socketService: SocketService
    
    /// Выбранные типы
    private var selectedTypes: [QuotationType] {
        didSet {
            guard oldValue != selectedTypes else {
                return
            }
            try? storage.save(selectedTypes, for: selectedTypesCacheKey)
        }
    }
    
    /// Список котировок
    private var quotations: [QuotationType: QuotationTick] = [:]
    
    // MARK: - lifecycle
    
    init(storage: Storage, socketService: SocketService) {
        self.storage = storage
        self.socketService = socketService
        self.selectedTypes = storage.load(for: selectedTypesCacheKey)
        
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.background
        tableView.register(QuotationCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.tableFooterView = UIView()
        
        let headerView = QuotationCell(style: .default, reuseIdentifier: nil)
        headerView.configure(with: ["Symbol", "BID / ASK", "SPREAD"])
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)
        tableView.tableHeaderView = headerView
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                           target: self,
                                                           action: #selector(showSelector))
        setEditing(false, animated: false)
        
        socketService.start(delegate: self) { [weak self] in
            self?.updateSubscription()
        }
    }

    // MARK - actions
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        tableView.setEditing(editing, animated: animated)
        navigationItem.rightBarButtonItem = isEditing
            ? UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveEditing))
            : UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(enterEditing))
    }
    
    /// Войти в режим редактирования
    @objc private func enterEditing() {
        setEditing(true, animated: true)
    }
    
    /// Выйти из режима редактирования
    @objc private func saveEditing() {
        setEditing(false, animated: true)
    }
    
    /// Обновить подписку
    private func updateSubscription() {
        socketService.subscribe(types: selectedTypes)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedTypes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        configure(cell: cell, at: indexPath)

        return cell
    }
    
    /// Заполнение ячейки по индекспафу
    ///
    /// - Parameters:
    ///   - cell: Ячейка
    ///   - indexPath: Индекспаф
    private func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        if let cell = cell as? QuotationCell {
            let type = selectedTypes[indexPath.row]
            let quotation = quotations[type]
            let first = type.displayName
            let second = quotation.flatMap { "\($0.bid) / \($0.ask)" } ?? "-"
            let third = quotation.flatMap { "\($0.spread)" } ?? "-"
            cell.configure(with: [first, second, third])
            cell.selectionStyle = .none
        }
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            selectedTypes.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            updateSubscription()
        }
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let fromIndex = fromIndexPath.row
        let toIndex = to.row
        selectedTypes.insert(selectedTypes.remove(at: fromIndex), at: toIndex)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
        
}

// MARK: - Work with selector
extension QuotationsDisplayController: QuotationTypeSelectorDelegate {

    /// Отобразить выбор типов котировок
    @objc private func showSelector() {
        let selector = QuotationTypeSelector(availableTypes: QuotationType.all,
                                             selectedTypes: selectedTypes)
        selector.delegate = self
        let nc = UINavigationController(rootViewController: selector)
        present(nc, animated: true, completion: nil)
    }
    
    func quotationTypeSelectorDidCancel(_ vc: QuotationTypeSelector) {
        vc.dismiss(animated: true, completion: nil)
    }
    
    func quotationTypeSelectorDidSelect(_ vc: QuotationTypeSelector, types: [QuotationType]) {
        selectedTypes = types
        updateSubscription()
        tableView.reloadData()
        
        vc.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - SocketServiceDelegate
extension QuotationsDisplayController: SocketServiceDelegate {
    
    func socketServiceDidReceive(_ service: SocketService, response: SocketService.Response) {
        switch response {
        case .success(let ticks):
            ticks.forEach { tick in
                quotations[tick.type] = tick
            }
            for ip in tableView.indexPathsForVisibleRows ?? [] {
                guard let cell = tableView.cellForRow(at: ip) else {
                    continue
                }
                configure(cell: cell, at: ip)
            }
        case .error(let error):
            print("Ошибка получение данных с сокета: \(error)")
        }
    }
    
}
