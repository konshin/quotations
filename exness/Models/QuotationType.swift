//
//  QuotationType.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import Foundation

/// Типы котировок
enum QuotationType: String {
    case EURUSD
    case EURGBP
    case USDJPY
    case GBPUSD
    case USDCHF
    case USDCAD
    case AUDUSD
    case EURJPY
    case EURCHF
    
    /// Список всех типов
    static var all: [QuotationType] {
        return [.EURUSD,
                .EURGBP,
                .USDJPY,
                .GBPUSD,
                .USDCHF,
                .USDCAD,
                .AUDUSD,
                .EURJPY,
                .EURCHF]
    }
}

// MARK: - extra
extension QuotationType {
    
    /// Имя для отображения
    var displayName: String {
        let middleIndex = rawValue.index(rawValue.startIndex, offsetBy: rawValue.count / 3 + 1)
        let prefix = rawValue.prefix(upTo: middleIndex)
        let suffix = rawValue.suffix(from: middleIndex)
        return "\(prefix) / \(suffix)"
    }
    
    /// Ключ для типа
    var key: String { return rawValue }
    
}

// MARK: - Codable
extension QuotationType: Codable {}
