//
//  QuotationResponse.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import Foundation

/// Ответ от сервера
struct QuotationResponse {
    /// Количество подписок
    let subscribedCount: Int
    /// Список подписок
    let subscribedList: QuotationSubscribeList
}

// MARK: - QuotationResponse: Decodable
extension QuotationResponse: Decodable {
    
    /// Пары для парсинга
    private enum CodingKeys: String, CodingKey {
        case subscribedCount = "subscribed_count"
        case subscribedList = "subscribed_list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        subscribedCount = try container.decode(Int.self, forKey: .subscribedCount)
        subscribedList = try container.decode(QuotationSubscribeList.self, forKey: .subscribedList)
    }
    
}

/// МОдель списка подписки
struct QuotationSubscribeList: Decodable {
    /// Список котировок
    let ticks: [QuotationTick]?
}
