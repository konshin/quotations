//
//  QuotationTick.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import Foundation

/// Модель котировок
struct QuotationTick {
    let type: QuotationType
    let bid: Double
    let bf: Double
    let ask: Double
    let af: Double
    let spread: Double
}

// MARK: - Decodable
extension QuotationTick: Decodable {
    
    /// Пары для парсинга
    private enum CodingKeys: String, CodingKey {
        case type = "s"
        case bid = "b"
        case bf
        case ask = "a"
        case af
        case spread = "spr"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(QuotationType.self, forKey: .type)
        bid = try container.decode(String.self, forKey: .bid).toDouble()
        bf = try container.decode(Double.self, forKey: .bf)
        ask = try container.decode(String.self, forKey: .ask).toDouble()
        af = try container.decode(Double.self, forKey: .af)
        spread = try container.decode(String.self, forKey: .spread).toDouble()
    }
    
}

// MARK: - Convertation to Double
private extension String {
    
    /// Ошибка конвертции
    private enum ConvertError: Error {
        case cannotConvert
    }
    
    func toDouble() throws -> Double {
        guard let double = Double(self) else {
            throw ConvertError.cannotConvert
        }
        return double
    }
    
}
