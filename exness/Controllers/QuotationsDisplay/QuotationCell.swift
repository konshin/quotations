//
//  QuotationCell.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import UIKit

/// Ячейка котировки
class QuotationCell: UITableViewCell {
    
    /// Стеквью для лейбл
    fileprivate let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = UIStackViewDistribution.fillEqually
        view.alignment = .fill
        
        return view
    }()
    
    /// Список лейбл
    private var labels: [UILabel] = []

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Configuration
extension QuotationCell {
    
    /// Конфигурация ячейки
    ///
    /// - Parameter items: Итемы для заполнения
    func configure(with items: [String]) {
        setNumberOfLabels(items.count)
        zip(items, labels).forEach { item, label in
            label.text = item
        }
    }
    
    /// Задает определенное количество лейбл
    ///
    /// - Parameter number: Количество
    private func setNumberOfLabels(_ number: Int) {
        if labels.count < number {
            for _ in 0..<number - labels.count {
                let label = UILabel()
                label.textAlignment = .center
                label.font = UIFont.systemFont(ofSize: 11)
                label.textColor = UIColor.text
                labels.append(label)
                stackView.addArrangedSubview(label)
            }
        } else if labels.count > number {
            let extraLabels = labels.suffix(from: number)
            extraLabels.forEach { label in
                labels.remove(at: number)
                stackView.removeArrangedSubview(label)
            }
        }
    }
    
}
