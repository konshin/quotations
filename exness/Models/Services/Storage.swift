//
//  Storage.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import Foundation

/// Хранилище данных
class Storage {
    
    /// Юзер дефолты
    private let userDefaults = UserDefaults.standard
    
    /// Сохранить объект
    ///
    /// - Parameters:
    ///   - object: Объект
    ///   - key: Ключ хранения
    func save<T: Encodable>(_ objects: [T], for key: String) throws {
        let coder = JSONEncoder()
        let data = try coder.encode(objects)
        userDefaults.set(data, forKey: key)
    }
    
    /// Загрузить объект из хранилища
    ///
    /// - Parameter key: Ключ хранения
    /// - Returns: Объект, если есть
    func load<T: Decodable>(for key: String) -> [T] {
        let decoder = JSONDecoder()
        if let data = userDefaults.data(forKey: key), let objects: [T] = try? decoder.decode([T].self, from: data) {
            return objects
        }
        return []
    }
    
}
