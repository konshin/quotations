//
//  QuotationTypeSelector.swift
//  exness
//
//  Created by Алексей Коньшин on 03.04.2018.
//  Copyright © 2018 Алексей Коньшин. All rights reserved.
//

import UIKit

/// Идентификатор для ячейки
private let reuseIdentifier = "reuseIdentifier"

/// Протокол общения с внешним миром
protocol QuotationTypeSelectorDelegate: class {
    
    /// Выбран список типов котировок
    ///
    /// - Parameters:
    ///   - vc: Экран выбора
    ///   - types: Список выбранных котировок
    func quotationTypeSelectorDidSelect(_ vc: QuotationTypeSelector, types: [QuotationType])
    
    /// Отменен выбор котировок
    ///
    /// - Parameters:
    ///   - vc: Экран выбора
    func quotationTypeSelectorDidCancel(_ vc: QuotationTypeSelector)
    
}

/// Экран выбора котировок
class QuotationTypeSelector: UITableViewController {
    
    /// Список выбранных котировок
    private var selectedTypes: [QuotationType]
    /// Список типов котировок
    private var types: [QuotationType]
    
    // MARK: - properties
    
    /// делегат
    weak var delegate: QuotationTypeSelectorDelegate?
    
    // MARK: - lifecycle
    
    init(availableTypes: [QuotationType], selectedTypes: [QuotationType]) {
        self.types = availableTypes
        self.selectedTypes = selectedTypes
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.background
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.tableFooterView = UIView()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                                                           target: self,
                                                           action: #selector(cancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(done))
    }
    
    // MARK: - actions
    
    /// Отмена выбора
    @objc private func cancel() {
        delegate?.quotationTypeSelectorDidCancel(self)
    }
    
    /// Завершить выбор
    @objc private func done() {
        delegate?.quotationTypeSelectorDidSelect(self,
                                                 types: selectedTypes)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        let type = types[indexPath.row]
        let isSelected = selectedTypes.contains(type)
        cell.textLabel?.text = type.displayName
        cell.textLabel?.textColor = UIColor.text
        cell.accessoryType = isSelected ? .checkmark : .none
        cell.backgroundColor = UIColor.clear
        
        cell.selectionStyle = .none

        return cell
    }

    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = types[indexPath.row]
        if let index = selectedTypes.index(of: type) {
            selectedTypes.remove(at: index)
        } else {
            selectedTypes.append(type)
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }

}
